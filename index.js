const Joi = require('joi');
const express = require('express');
const app = express();

require('./startup/prod')(app);

app.use(express.json());

let heroes = [
  {
    name: 'Luke Skywalker',
    height: '172',
    mass: '77',
    hair_color: 'blond',
    side: 'light',
    skin_color: 'fair',
    eye_color: 'blue',
    birth_year: '19BBY',
    gender: 'male'
  },
  {
    name: 'R2-D2',
    height: '96',
    mass: '32',
    hair_color: 'n/a',
    side: 'light',
    skin_color: 'white, blue',
    eye_color: 'red',
    birth_year: '33BBY',
    gender: 'n/a'
  },
  {
    name: 'Darth Vader',
    height: '202',
    mass: '136',
    hair_color: 'none',
    skin_color: 'white',
    eye_color: 'yellow',
    birth_year: '41.9BBY',
    gender: 'male',
    side: 'dark'
  },
  {
    name: 'Obi-Wan Kenobi',
    height: '182',
    mass: '77',
    hair_color: 'auburn, white',
    skin_color: 'fair',
    eye_color: 'blue-gray',
    birth_year: '57BBY',
    gender: 'male',
    side: 'light'
  }
];

app.post('/api/heroes', (req, res) => {
  const { error } = validateSearch(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let searchResult = [];

  heroes.forEach(hero => {
    let heroName = hero.name.toLowerCase();

    if (!req.query.search) {
      searchResult.push(hero)
    }

    if (req.query.search && heroName.includes(req.query.search.toLowerCase())) {
      searchResult.push(hero)
    }
  });

  if (req.query.side && req.query.side.length) {
    searchResult = searchResult.filter(result => result.side === req.query.side.toLowerCase())
  }

  res.send(searchResult);
});

function validateSearch(genre) {
  const schema = {
    search: Joi.string(),
    side: Joi.string(),
  };

  return Joi.validate(genre, schema);
}

const port = process.env.PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}...`));